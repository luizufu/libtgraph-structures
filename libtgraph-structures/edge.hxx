#pragma once

#include <bits/stdint-uintn.h>
#include <sstream>
#include <tuple>

namespace tgraph_structures
{
struct edge
{
    uint32_t u;
    uint32_t v;
    uint32_t t;
};

inline auto operator==(const edge& lhs, const edge& rhs) -> bool
{
    return lhs.u == rhs.u && lhs.v == rhs.v;
}

inline auto operator!=(const edge& lhs, const edge& rhs) -> bool
{
    return !(lhs == rhs);
}

inline auto operator<(const edge& lhs, const edge& rhs) -> bool
{
    return std::tie(lhs.u, lhs.v) < std::tie(rhs.u, rhs.v);
}

inline auto operator<<(std::ostream& out, const edge& e) -> std::ostream&
{
    out << e.u << ' ' << e.v;
    return out;
}

inline auto operator>>(std::istream& in, edge& e) -> std::istream&
{
    in >> e.u >> e.v;
    return in;
}

} // namespace tgraph_structures
