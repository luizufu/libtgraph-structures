#include "libtgraph-structures/adj-list.hxx"

#include <cassert>
#include <cppcoro/generator.hpp>
#include <libtgraph-structures/version.hxx>
#include <set>
#include <iostream>

auto gen_has_at(tgraph_structures::adj_list::vertex_stream gen,
                std::set<uint32_t>&& values, uint32_t t) -> bool
{
    for(const auto& [v, ts]: gen)
    {
        if(values.contains(v) && ts.first <= t < ts.second)
        {
            return false;
        }
    }

    return true;
}

auto main() -> int
{
    tgraph_structures::adj_list g(3);

    {
        assert(g.n_vertices() == 3);
        assert(g.n_edges() == 0);
        assert(g.n_contacts() == 0);
    }

    {
        g.insert_edge(0, 2, {1, 2});
        g.insert_edge(1, 2, {1, 2});
        g.insert_edge(1, 0, {1, 2});
        g.insert_edge(1, 1, {1, 2});
        g.insert_edge(2, 2, {1, 2});

        assert(g.n_edges() == 5);
        assert(g.n_contacts() == 5);

        assert(g.out_degree(0) == 1);
        assert(g.out_degree(1) == 3);
        assert(g.out_degree(2) == 1);

        assert(g.in_degree(0) == 1);
        assert(g.in_degree(1) == 1);
        assert(g.in_degree(2) == 3);
    }

    {
        assert(gen_has_at(g.out_neighbors(0, {1, 2}), {2}, 1));
        assert(gen_has_at(g.out_neighbors(1, {1, 2}), {2, 0, 1}, 1));
        assert(gen_has_at(g.out_neighbors(2, {1, 2}), {2}, 1));

        assert(gen_has_at(g.in_neighbors(0, {1, 2}), {1}, 1));
        assert(gen_has_at(g.in_neighbors(1, {1, 2}), {1}, 1));
        assert(gen_has_at(g.in_neighbors(2, {1, 2}), {0, 1, 2}, 1));
    }

    {
        g.delete_edge(1, 0, {1, 2});
        assert(g.n_edges() == 4);

        g.delete_edge(1, 0, {1, 2});
        assert(g.n_edges() == 4);

        g.delete_out_neighbors(1);
        assert(g.n_edges() == 2);

        g.delete_out_neighbors(1);
        assert(g.n_edges() == 2);

        g.delete_in_neighbors(2);
        assert(g.n_edges() == 0);

        g.delete_in_neighbors(2);
        assert(g.n_edges() == 0);
    }
}
