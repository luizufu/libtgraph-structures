#pragma once

#include <cppcoro/generator.hpp>
#include <set>
#include <unordered_map>
#include <vector>

namespace tgraph_structures
{
class adj_list
{
public:
    using interval = std::pair<uint32_t, uint32_t>;
    using vertex_stream = cppcoro::generator<std::pair<uint32_t, interval>>;
    using edge_stream =
        cppcoro::generator<std::tuple<uint32_t, uint32_t, interval>>;

    explicit adj_list(size_t n);

    void insert_edge(uint32_t u, uint32_t v, interval ts);
    void delete_edge(uint32_t u, uint32_t v, interval ts);

    void delete_out_neighbors(uint32_t u);
    void delete_in_neighbors(uint32_t v);

    [[nodiscard]] auto has_edge(uint32_t u, uint32_t v, interval ts) const
        -> bool;

    [[nodiscard]] auto out_neighbors(uint32_t u, interval ts) const
        -> vertex_stream;
    [[nodiscard]] auto in_neighbors(uint32_t v, interval ts) const
        -> vertex_stream;
    [[nodiscard]] auto edges(interval ts) const -> edge_stream;

    [[nodiscard]] auto out_degree(uint32_t u) const -> size_t;
    [[nodiscard]] auto in_degree(uint32_t v) const -> size_t;

    [[nodiscard]] auto n_vertices() const -> size_t;
    [[nodiscard]] auto n_edges() const -> size_t;
    [[nodiscard]] auto n_contacts() const -> size_t;

private:
    std::vector<std::unordered_map<uint32_t, std::set<uint32_t>>> _list;
    std::vector<size_t> _out_contacts;
    size_t _n_edges;
    size_t _n_contacts;
};

} // namespace tgraph_structures
