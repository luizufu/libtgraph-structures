#pragma once

// The numeric version format is AAAAABBBBBCCCCCDDDE where:
//
// AAAAA - major version number
// BBBBB - minor version number
// CCCCC - bugfix version number
// DDD   - alpha / beta (DDD + 500) version number
// E     - final (0) / snapshot (1)
//
// When DDDE is not 0, 1 is subtracted from AAAAABBBBBCCCCC. For example:
//
// Version      AAAAABBBBBCCCCCDDDE
//
// 0.1.0        0000000001000000000
// 0.1.2        0000000001000020000
// 1.2.3        0000100002000030000
// 2.2.0-a.1    0000200001999990010
// 3.0.0-b.2    0000299999999995020
// 2.2.0-a.1.z  0000200001999990011
//
#define LIBTGRAPH_STRUCTURES_VERSION       $libtgraph_structures.version.project_number$ULL
#define LIBTGRAPH_STRUCTURES_VERSION_STR   "$libtgraph_structures.version.project$"
#define LIBTGRAPH_STRUCTURES_VERSION_ID    "$libtgraph_structures.version.project_id$"
#define LIBTGRAPH_STRUCTURES_VERSION_FULL  "$libtgraph_structures.version$"

#define LIBTGRAPH_STRUCTURES_VERSION_MAJOR $libtgraph_structures.version.major$
#define LIBTGRAPH_STRUCTURES_VERSION_MINOR $libtgraph_structures.version.minor$
#define LIBTGRAPH_STRUCTURES_VERSION_PATCH $libtgraph_structures.version.patch$

#define LIBTGRAPH_STRUCTURES_PRE_RELEASE   $libtgraph_structures.version.pre_release$

#define LIBTGRAPH_STRUCTURES_SNAPSHOT_SN   $libtgraph_structures.version.snapshot_sn$ULL
#define LIBTGRAPH_STRUCTURES_SNAPSHOT_ID   "$libtgraph_structures.version.snapshot_id$"
