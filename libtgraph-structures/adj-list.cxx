#include <bits/stdint-uintn.h>
#include <libtgraph-structures/adj-list.hxx>

namespace tgraph_structures
{
adj_list::adj_list(size_t n)
    : _list(n)
    , _out_contacts(n)
    , _n_edges(0)
    , _n_contacts(0)
{
}

void adj_list::insert_edge(uint32_t u, uint32_t v, interval ts)
{
    auto& adj = _list[u];

    auto inserted = adj.insert({ v, {} });
    if(inserted.second)
    {
        ++_n_edges;
    }

    for(uint32_t t = ts.first; t < ts.second; ++t)
    {
        if(inserted.first->second.insert(t).second)
        {
            ++_out_contacts[u];
            ++_n_contacts;
        }
    }
}

void adj_list::delete_edge(uint32_t u, uint32_t v, interval ts)
{
    auto& adj = _list[u];
    auto it = adj.find(v);

    if(it != adj.end())
    {
        for(uint32_t t = ts.first; t < ts.second; ++t)
        {
            if(it->second.erase(t) > 0)
            {
                --_out_contacts[u];
                --_n_contacts;
            }
        }

        if(it->second.empty())
        {
            adj.erase(it);
            --_n_edges;
        }
    }
}

void adj_list::delete_out_neighbors(uint32_t u)
{
    _n_edges -= _list[u].size();
    _n_contacts -= _out_contacts[u];
    _list[u].clear();
}

void adj_list::delete_in_neighbors(uint32_t v)
{
    for(uint32_t u = 0; u < _list.size(); ++u)
    {
        auto it = _list[u].find(v);
        if(it != _list[u].end())
        {
            _n_contacts -= it->second.size();
            _out_contacts[u] -= it->second.size();
            --_n_edges;
            _list[u].erase(it);
        }
    }
}

auto adj_list::has_edge(uint32_t u, uint32_t v, interval ts) const -> bool
{
    const auto& adj = _list.at(u);
    auto it = adj.find(v);

    for(uint32_t t = ts.first; t < ts.second; ++t)
    {
        if(it->second.contains(t))
        {
            return true;
        }
    }

    return false;
}

auto adj_list::out_neighbors(uint32_t u, interval ts) const -> vertex_stream
{
    for(const auto& [v, times]: _list.at(u))
    {
        if(auto it = times.lower_bound(ts.first); it != times.end())
        {
            uint32_t t1 = *it;
            uint32_t t2 = t1;
            ++it;

            for(; it != times.end() && *it < ts.second; t2 = *it, ++it)
            {
                if(*it - t2 > 1)
                {
                    co_yield {v, {t1, t2}};
                    t1 = *it;
                }
            }

            co_yield {v, {t1, t2}};
        }
    }
}

auto adj_list::in_neighbors(uint32_t v, interval ts) const -> vertex_stream
{
    for(uint32_t u = 0; u < _list.size(); ++u)
    {
        const auto& adj = _list.at(u);
        auto it = adj.find(v);

        if(it != adj.end())
        {
            const auto& times = it->second;
            if(auto it2 = times.lower_bound(ts.first); it2 != times.end())
            {
                uint32_t t1 = *it2;
                uint32_t t2 = t1;
                ++it;

                for(; it2 != times.end() && *it2 < ts.second; t2 = *it2, ++it2)
                {
                    if(*it2 - t2 > 1)
                    {
                        co_yield {u, {t1, t2}};
                    }
                }

                co_yield {u, {t1, t2}};
            }
        }
    }
}

auto adj_list::edges(interval ts) const -> edge_stream
{
    for(uint32_t u = 0; u < _list.size(); ++u)
    {
        for(const auto& [v, times]: _list.at(u))
        {
            if(auto it = times.lower_bound(ts.first); it != times.end())
            {
                uint32_t t1 = *it;
                uint32_t t2 = t1;
                ++it;

                for(; it != times.end() && *it < ts.second; t2 = *it, ++it)
                {
                    if(*it - t2 > 1)
                    {
                        co_yield {u, v, {t1, t2}};
                        t1 = *it;
                    }
                }

                co_yield {u, v, {t1, t2}};
            }
        }
    }
}

auto adj_list::out_degree(uint32_t u) const -> size_t
{
    return _list[u].size();
}

auto adj_list::in_degree(uint32_t v) const -> size_t
{
    size_t degree = 0;
    for(const auto& u: _list)
    {
        if(u.contains(v))
        {
            ++degree;
        }
    }

    return degree;
}

auto adj_list::n_vertices() const -> size_t
{
    return _list.size();
}

auto adj_list::n_edges() const -> size_t
{
    return _n_edges;
}

auto adj_list::n_contacts() const -> size_t
{
    return _n_contacts;
}

} // namespace tgraph_structures
